import _ from 'lodash'
import React, { PureComponent } from 'react'
import { StyleSheet, View, ImageBackground, Text } from 'react-native'
import { connect } from 'react-redux'
import { Field, ChatItem, ChatSendButton, Spinner, ChatInvitationScreen } from './common'
import { chatChanged, sendChat, chatsFetch, acceptChatRequest } from '../actions'
import ReversedFlatList from 'react-native-reversed-flat-list'
import firebase from 'firebase'

class ChatsScreen extends PureComponent {

    componentDidMount() {
        this.props.chatsFetch({ chatId: this.props.chatId })
    }

    onChatChanged(text) {
        this.props.chatChanged(text)
    }

    onSendButtonPressed() {
        if (this.props.message !== '') {
            this.props.sendChat({ text: this.props.message, chatId: this.props.chatId })
        } else {
            console.log("Chats empty")
        }
    }

    renderItem(chat) {
        return <ChatItem chat={chat} />
    }

    getBodyComponent() {
        if (this.props.isLoading) {
            return <Spinner size='large' pStyles={{
                flex: 1,
                justifyContent: 'center',
                alignSelf: 'center',
                margin: 16
            }} />
        } else {
            return <ImageBackground source={require('../assets/background.jpg')} style={styles.imageStyle}>
                <ReversedFlatList
                    style={{ backgroundColor: 'rgba(255,255,255,0.8)' }}
                    data={this.props.chatsList}
                    renderItem={this.renderItem}
                    keyExtractor={(chat) => chat._id} />
            </ImageBackground >
        }
    }

    FlatListRef = null;

    renderScreen() {
        const { chatSendContainer, mainContainer, listContainer } = styles
        if (this.props.isLoading) {
            return <Spinner size='large' pStyles={{
                flex: 1,
                justifyContent: 'center',
                alignSelf: 'center',
                margin: 16
            }} />
        } else if (!_.isEmpty(this.props.chats)) {
            if (this.props.chats.acceptedByRecepient === true) {
                return <View style={mainContainer}>
                    <View style={listContainer}>
                        {this.getBodyComponent()}
                    </View>
                    <View style={chatSendContainer}>
                        <Field placeholder="Type a message.."
                            onChangeText={this.onChatChanged.bind(this)}
                            value={this.props.message} />
                        <ChatSendButton onPress={this.onSendButtonPressed.bind(this)} />
                    </View>
                </View >
            } else {
                const { currentUser } = firebase.auth()
                if (this.props.chats.invitedBy === currentUser.uid) {
                    var message = `You can chat with ${this.props.chatName} once they accept your request!`
                    return <View style={mainContainer}><ChatInvitationScreen messageText={message} buttonVisibility={false} /></View>
                } else {
                    var message = `${this.props.chatName} wants to chat with you, click on Accept button to start chatting!`
                    return <View style={mainContainer}><ChatInvitationScreen messageText={message} buttonVisibility={true} onPress={this.acceptChatRequest.bind(this)} /></View>
                }
            }
        } else {
            console.log('--->>> ChatsEmpty')
        }

    }

    acceptChatRequest() {
        this.props.acceptChatRequest({ chatId: this.props.chatId })
    }

    render() {
        return <View style={styles.mainContainer}>
            {this.renderScreen()}
        </View>
    }
}

const styles = StyleSheet.create({
    chatSendContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8,
        marginBottom: 8
    },
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    listContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    imageStyle: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    }
})

const mapsStateToProps = ({ chat }) => {
    const { message, chats, isLoading } = chat
    const chatsList = _.map(chats.messages, (val, _id) => {
        return { ...val, _id }
    })
    return { message, chats, chatsList, isLoading }
}

export default connect(mapsStateToProps, { chatChanged, sendChat, chatsFetch, acceptChatRequest })(ChatsScreen)