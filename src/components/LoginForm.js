import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { emailChanged, passwordChanged, loginUser } from '../actions'
import { Button, Card, CardSection, Field, Spinner } from './common'

class LoginForm extends PureComponent {

    onEmailChange(text) {
        this.props.emailChanged(text)
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text)
    }

    onButtonPress() {
        const { email, password } = this.props
        this.props.loginUser({ email, password })
    }

    renderError() {
        if (this.props.error) {
            return <View>
                <Text style={styles.errorTextStyle}>{this.props.error}</Text>
            </View>
        }
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size='small' pStyles={styles.spinnerStyle}/>
        }

        return <Button title="Log In" onPress={this.onButtonPress.bind(this)} />

    }

    render() {
        return <Card>

            <CardSection>
                <Field
                    value={this.props.email}
                    label="Email"
                    placeholder="email@gmail.com"
                    onChangeText={this.onEmailChange.bind(this)}
                />
            </CardSection>

            <CardSection>
                <Field
                    secureTextEntry
                    label="Password"
                    placeholder="Password"
                    onChangeText={this.onPasswordChange.bind(this)}
                    value={this.props.password}
                />
            </CardSection>

            {this.renderError()}

            <CardSection>
                {this.renderButton()}
            </CardSection>

            {/* <CardSection>
                <View style={styles.signUpViewStyle}>
                    <Text>Dont have an Account?</Text>
                    <TouchableOpacity onPress={()=>{Actions.signup()}}>
                    <Text style={styles.signUpTextStyle}> Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </CardSection> */}

        </Card>
    }
}

const mapsStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth
    return { email, password, error, loading }
}

const styles = StyleSheet.create({
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        margin:16
    },
    signUpViewStyle:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center'
    },
    signUpTextStyle:{
        fontWeight:'bold',
        color:'#2196F3'
    }
})

export default connect(mapsStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm)