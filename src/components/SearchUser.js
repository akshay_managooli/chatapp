import _ from 'lodash'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { View, FlatList } from 'react-native'
import { CardSection, Card, Field, Spinner } from './common'
import { fetchUsers } from '../actions'
import UserListItem from './common/UserListItem'
import firebase from 'firebase'
class SearchUser extends PureComponent {

    state = { filteredUsers: [] }

    componentDidMount() {
        if (_.isEmpty(this.props.allUsersList)) {
            this.props.fetchUsers({ isSetCurrentUser: false })
        }
    }

    onSearchChanged(text) {
        if (text !== '') {
            const { currentUser } = firebase.auth()
            const filteredUsers = this.props.allUsersList.filter((item) => {
                if (item.uid === currentUser.uid) {
                    return false
                } else {
                    return item.username.toLowerCase().includes(text.toLowerCase()) || 
                    item.email.toLowerCase().includes(text.toLowerCase())
                }
            })



            if (!_.isEmpty(filteredUsers)) {
                this.setState({ filteredUsers: filteredUsers })
            } else {
                this.setState({ filteredUsers: [] })
            }
        } else {
            this.setState({ filteredUsers: [] })
        }
    }

    renderItem(user) {
        return <UserListItem user={user} />
    }

    renderFlatList() {
        if (!_.isEmpty(this.state.filteredUsers)) {
            return <CardSection>

                <FlatList
                    data={this.state.filteredUsers}
                    renderItem={this.renderItem}
                    keyExtractor={(user) => user.uid}
                />
            </CardSection>
        } else {
            return null
        }
    }

    renderScreen() {
        if (this.props.isLoading) {
            return <Spinner size="large" pStyles={{
                flex: 1,
                justifyContent: 'center',
                alignSelf: 'center',
                margin: 16
            }} />
        } else {
            return <Card>
                <CardSection>
                    <Field
                        label="Search"
                        placeholder="Search by username or email"
                        onChangeText={this.onSearchChanged.bind(this)}
                    />
                </CardSection>
                {this.renderFlatList()}
            </Card>
        }
    }

    render() {
        return <View style={{
            flex: 1,
            flexDirection: 'column'
        }}>
            {this.renderScreen()}
        </View>
    }
}

const mapsStateToProps = ({ chat }) => {
    const { allusers, isLoading, currentUser } = chat
    const allUsersList = _.map(allusers, (val, _id) => {
        return { ...val, _id }
    })
    return { allUsersList, isLoading, currentUser }
}

export default connect(mapsStateToProps, { fetchUsers })(SearchUser)