import React, { PureComponent } from 'react'
import { Image, StyleSheet, View } from 'react-native'
import { Spinner } from './common'
import firebase from 'firebase'

class SplashScreen extends PureComponent {

    render() {

        const { mainContainer, imageView } = styles

        return <View style={mainContainer}>
            <Image style={imageView} source={require('../assets/chat.png')} />
            <Spinner size="large" />
        </View>
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',

    },
    imageView: {
        height: 100,
        width: 100
    }
})

export default SplashScreen