import React from 'react'
import { TouchableOpacity, Image, StyleSheet } from 'react-native'

const ChatSendButton = ({ onPress }) => {
    return <TouchableOpacity style={style.buttonStyle}
        onPress={onPress}>
        <Image source={require('../../assets/send.png')} />
    </TouchableOpacity>
}

const style = StyleSheet.create({
    buttonStyle: {
        backgroundColor: '#2196F3',
        borderRadius: 25,
        margin: 8,
        height: 40,
        width: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export { ChatSendButton }