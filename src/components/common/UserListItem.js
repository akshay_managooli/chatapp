import React,{PureComponent} from 'react'
import { connect } from 'react-redux'
import { TouchableWithoutFeedback, View, Text, StyleSheet, Image } from 'react-native'
import {createChatRoom} from '../../actions'

class UserListItem extends PureComponent {
    render(){
        const { mainContainer, imageStyle, textStyle } = styles
        const{username} = this.props.user.item
        const {item} = this.props.user
        const currentUserObjectid = this.props.chat.currentUser._id
        return <TouchableWithoutFeedback onPress={()=>{
            this.props.createChatRoom({recepientUser:item,currentUserObjectid})
        }}>
            <View style={mainContainer}>
                <Image
                    style={imageStyle}
                    source={require('../../assets/chat.png')}
                />
                <Text style={textStyle}> {username}</Text>
            </View>
        </TouchableWithoutFeedback>
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center'
    },
    imageStyle: {
        width: 35,
        height: 35,
        borderRadius: 20,
        margin: 8,
        alignSelf: 'center'
    },
    textStyle: {
        marginEnd: 8,
        alignSelf: 'center',
        color: '#000',
        fontSize: 14
    }
})

const mapsStateToProps = ({chat}) =>{
    return {chat}
}

export default connect(mapsStateToProps,{createChatRoom})(UserListItem)