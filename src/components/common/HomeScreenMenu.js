import React from 'react'
import { Text, View, Modal, StyleSheet, TouchableOpacity } from 'react-native'
import { CardSection } from './CardSection'
import { Button } from './Button'

const HomeScreenMenu = ({ visible , onOptionPress}) => {
    const { containerStyle, textStyle, subContainer ,optionTextStyle} = style
    return <Modal
        visible={visible}
        animationType={'slide'}
        onRequestClose={() => { }}
        transparent>
        <View style={containerStyle}>
            <View style={subContainer}>
            <TouchableOpacity onPress={()=>{onOptionPress('profile')}}>
                    <Text style={textStyle}>Profile</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{onOptionPress('logout')}}>
                    <Text style={textStyle}>Logout</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{onOptionPress('cancel')}}>
                    <Text style={optionTextStyle}>Cancel</Text>
                </TouchableOpacity>
            </View>
        </View >
    </Modal >
}

const style = StyleSheet.create({
    textStyle: {
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40,
        borderBottomColor:'#ddd',
        borderBottomWidth:0.5,
        fontWeight:'bold'
    },
    optionTextStyle: {
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40,
        borderBottomColor:'#ddd',
        borderBottomWidth:0.5,
        fontWeight:'bold',
        color:'#2196F3'
    },
    containerStyle: {
        backgroundColor: 'rgba(0,0,0,0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'flex-end'
    }, subContainer: {
        backgroundColor: '#fff',
        borderTopStartRadius: 8,
        borderTopEndRadius: 8,
        padding: 16
    }
})

export { HomeScreenMenu }
