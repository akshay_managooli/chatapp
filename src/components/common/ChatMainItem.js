import React, { PureComponent } from 'react'
import { TouchableWithoutFeedback, View, Text, StyleSheet, Image } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { CardSection } from './CardSection'

class ChatMainItem extends PureComponent {
    render() {
        const { mainContainer, imageStyle, textStyle } = styles
        const { chatName, chatId } = this.props.chat.item
        return <TouchableWithoutFeedback onPress={() => {
            Actions.chatsScreen({ chatId: chatId ,chatName:chatName})
        }}>
                <View style={mainContainer}>
                    <Image
                        style={imageStyle}
                        source={require('../../assets/chat.png')}
                    />
                    <Text style={textStyle}> {chatName}</Text>
                </View>
        </TouchableWithoutFeedback>
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
        borderColor:'#ddd',
        borderBottomWidth:1
    },
    imageStyle: {
        width: 35,
        height: 35,
        borderRadius: 20,
        margin: 16,
        alignSelf: 'center'
    },
    textStyle: {
        marginEnd: 8,
        alignSelf: 'center',
        color: '#000',
        fontSize: 16
    }
})

export default ChatMainItem