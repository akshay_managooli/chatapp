import React, { PureComponent } from 'react'
import { FlatList } from 'react-native'
import { connect } from 'react-redux'
import ChatItem from './ChatItem'

class ChatListComponent extends PureComponent {

    renderItem(chat){
        return <ChatItem chat={chat}/>
    }

    render() {
        return <FlatList 
            data={this.props.chats}
            renderItem={this.renderItem}
            keyExtractor={(chat)=>`${chat.id}`}
        />
    }
}

const mapsStateProps = state => {
    return { chats: state.chats }
}

export default connect(mapsStateProps)(ChatListComponent)