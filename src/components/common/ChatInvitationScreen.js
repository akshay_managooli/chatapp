import React from 'react'
import { Text, View, StyleSheet ,Image} from 'react-native'
import { Button } from './Button'
import { CardSection } from './CardSection'

const ChatInvitationScreen = ({ onPress, messageText, buttonVisibility = false }) => {
    return <View style={styles.mainContainer}>
        <Image style={styles.imageView} source={require('../../assets/chat.png')} />
        <Text style={styles.emptyTextHeaderStyle}>{messageText}</Text>
        {getButton({ buttonVisibility, onPress })}
    </View>
}

const getButton = ({ buttonVisibility, onPress }) => {
    if (buttonVisibility) {
        return <CardSection><Button onPress={onPress} title="Accept" /></CardSection>
    } else {
        return <View></View>
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems:'center',
        justifyContent:'center'
    },
    emptyViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    emptyTextHeaderStyle: {
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    imageView: {
        height: 100,
        width: 100,
        alignSelf: 'center',
        marginBottom: 16
    }
})

export { ChatInvitationScreen }