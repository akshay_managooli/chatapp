import React, { Component } from 'react'
import {
    Text,
    View,
    StyleSheet,
    LayoutAnimation,
    TouchableWithoutFeedback,
    UIManager, Image
} from 'react-native'
import firebase from 'firebase'
import Moment from 'moment'
class ChatItem extends Component {
    componentDidMount() {
        UIManager.setLayoutAnimationEnabledExperimental(true)
    }

    componentDidUpdate() {
        LayoutAnimation.spring()
    }

    getCurrentUserId() {
        const { currentUser } = firebase.auth()
        return currentUser.uid
    }

    getFormatedTime(date) {
        return Moment(date).format('hh:mm a')
    }

    render() {
        const { titleStyle, messageStyle, mainContainerFlexEnd, mainContainerFlexStart, timeTextStyle, chatTextDetailContainer,imageBackgroundStyle } = styles
        const { name, message, id, date } = this.props.chat.item
        return <TouchableWithoutFeedback>
            <View style={this.getCurrentUserId() === id ? mainContainerFlexEnd : mainContainerFlexStart}>
                <Image source={require('../../assets/profile-user.png')} 
                style={imageBackgroundStyle}/>
                
                <View style={chatTextDetailContainer}>
                    <Text style={titleStyle}>
                        {name}
                    </Text>
                    <Text style={messageStyle}>
                        {message}
                    </Text>
                    <Text style={timeTextStyle}>
                        {this.getFormatedTime(date)}
                    </Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    }
}

const styles = StyleSheet.create({
    titleStyle: {
        color: '#000',
        fontSize: 16,
        paddingLeft: 8,
        fontWeight: 'bold'
    },
    messageStyle: {
        color: '#000',
        fontSize: 14,
        paddingLeft: 8
    },
    mainContainerFlexStart: {
        flex: 1,
        flexDirection: 'row',
        maxWidth: '70%',
        alignSelf: 'flex-start'
    },
    mainContainerFlexEnd: {
        alignSelf: 'flex-end',
        flex: 1,
        flexDirection: 'row',
        maxWidth: '70%'
    },
    timeTextStyle: {
        fontSize: 11,
        color: '#808080',
        alignSelf: 'flex-end'
    },
    chatTextDetailContainer: {

        backgroundColor: '#e3f3ff',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative',
        flexDirection: 'column',
        padding: 8,
        margin: 8,
        borderRadius: 8
    },
    imageBackgroundStyle: {
        backgroundColor: '#878787',
        borderRadius: 25,
        marginStart: 8,
        marginTop:8,
        height: 30,
        width: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export { ChatItem }