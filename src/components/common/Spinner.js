import React from 'react'
import { View, ActivityIndicator, StyleSheet } from 'react-native'

const Spinner = ({ size, pStyles }) => {
    return <View style={pStyles ? pStyles : style.spinnerStyle}>
        <ActivityIndicator size={size || 'large'} animating={true} color="#2196F3" />
    </View>
}

const style = StyleSheet.create({
    spinnerStyle: {
        alignSelf: 'center',
        margin: 16
    }
})

export { Spinner }