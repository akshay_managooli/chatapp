import lodash from 'lodash'
import React, { PureComponent } from 'react'
import { View, FlatList, StyleSheet, TouchableOpacity, Image, Text } from 'react-native'
import { FloatingAction } from 'react-native-floating-action'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { fetchUsers, fetchAllChats, logout ,usernameChanged,existingUserName} from '../actions'
import { Spinner, HomeScreenMenu } from './common'
import ChatMainItem from './common/ChatMainItem'


class ChatsMainListingScreen extends PureComponent {
    state = { showModal: false }

    componentDidMount() {
        setTimeout(() => {
            Actions.refresh({ right: this.renderRightButton() })
        }, 100)

        if (lodash.isEmpty(this.props.currentUser)) {
            this.props.fetchUsers({ isSetCurrentUser: true })
        } else {
            this.props.fetchAllChats({ currentUser: this.props.currentUser })
        }
    }

    renderRightButton = () => {
        return <TouchableOpacity onPress={() => this.handleIconTouch()}>
            <Image source={require('../assets/menu.png')}
                style={styles.menuIconStyle} />
        </TouchableOpacity>
    }

    handleIconTouch = () => {
        this.showModal()
    }

    fetchProfileAndNavigate = () => {
        console.log(this.props.currentUser)
        this.props.usernameChanged(this.props.currentUser.username)
        this.setState({ showModal: false })
        Actions.userdetails()
    }

    onOptionPress(param) {
        switch (param) {
            case 'profile':
                this.fetchProfileAndNavigate()
                break
            case 'logout':
                this.props.logout()
                break
            case 'cancel':
                this.setState({ showModal: false })
                break

            default:
                break;
        }
    }

    onFabActionClick(name) {
        switch (name) {
            case 'bt_new_chat':
                Actions.searchUser()
                break
            case 'bt_new_group':
                console.log('New Group not defined')
                break
            default:
                console.log('default')
                break
        }
    }

    renderItem(chat) {
        return <ChatMainItem chat={chat} />
    }

    renderScreen() {
        if (this.props.isLoading) {
            return <Spinner size="large" pStyles={{
                flex: 1,
                justifyContent: 'center',
                alignSelf: 'center',
                margin: 16
            }} />
        } else {
            if (!lodash.isEmpty(this.props.allChatsList)) {
                return <FlatList
                    data={this.props.allChatsList}
                    renderItem={this.renderItem}
                    keyExtractor={(chats) => chats.chatId}
                />
            } else {
                return <View style={styles.emptyViewStyle}>
                    <Image style={styles.imageView} source={require('../assets/chat.png')} />
                    <Text style={styles.emptyTextHeaderStyle}>Looks like you dont have any Chats</Text>
                    <View style={styles.emptyMessageViewStyle}>
                        <Text>Click on </Text>
                        <Text style={styles.plusTextStyle}>+</Text>
                        <Text > button to start new Chat</Text>
                    </View>

                </View>
            }
        }
    }

    showModal() {
        this.setState({ showModal: !this.state.showModal })
    }

    render() {
        const { mainContainer } = styles
        const actions = [
            {
                text: "New Group",
                icon: require("../assets/audience.png"),
                name: "bt_new_group",
                position: 2,
                color: "#2196F3"
            },
            {
                text: "New Chat",
                icon: require("../assets/user.png"),
                name: "bt_new_chat",
                position: 1,
                color: "#2196F3"
            }
        ];


        return <View style={mainContainer}>
            {this.renderScreen()}
            <FloatingAction
                position="right"
                icon={require('../assets/chat_fab_icon.png')}
                actions={actions}
                color="#2196F3"
                onPressItem={this.onFabActionClick.bind(this)}
            />
            <HomeScreenMenu
                visible={this.state.showModal}
                onOptionPress={this.onOptionPress.bind(this)}
            >ABCDED</HomeScreenMenu>
        </View>
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    menuIconStyle: {
        height: 20,
        width: 20,
        margin: 8
    },
    emptyViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    emptyTextHeaderStyle: {
        fontSize: 20,
        textAlign: 'justify',
        fontWeight: 'bold'
    },
    emptyTextMessageStyle: {
        fontSize: 16,
        textAlign: 'justify',
        fontWeight: '500',
        alignSelf: 'center'
    },
    imageView: {
        height: 100,
        width: 100,
        alignSelf: 'center',
        marginBottom: 16
    },
    plusTextStyle: {
        backgroundColor: '#2196F3',
        height: 20,
        width: 20,
        textAlign: 'center',
        color: '#fff',
        borderRadius: 25
    },
    emptyMessageViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop:6
    }
})

const mapsStateToProps = ({ chat }) => {
    const { currentUser, isLoading, allChats } = chat
    const allChatsList = lodash.map(allChats, (val, _id) => {
        return { ...val, _id }
    })
    return { currentUser, isLoading, allChatsList }
}

export default connect(mapsStateToProps, { fetchUsers, fetchAllChats, logout,usernameChanged,existingUserName })(ChatsMainListingScreen)