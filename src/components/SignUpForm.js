import React, { PureComponent } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { emailChanged, passwordChanged, signUpUser, usernameChanged } from '../actions'
import { Button, Card, CardSection, Field, Spinner } from './common'

class SignUpForm extends PureComponent {

    onEmailChange(text) {
        this.props.emailChanged(text)
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text)
    }

    onUsernameChange(text) {
        this.props.usernameChanged(text)
    }

    onButtonPress() {
        const { email, password, username } = this.props
        this.props.signUpUser({ email, password, username })
    }

    renderError() {
        if (this.props.error) {
            return <View>
                <Text style={styles.errorTextStyle}>{this.props.error}</Text>
            </View>
        }
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size='small' pStyles={styles.spinnerStyle} />
        }

        return <Button title="Sign Up" onPress={this.onButtonPress.bind(this)} />

    }

    render() {
        return <Card>

            <CardSection>
                <Field
                    value={this.props.username}
                    label="Username"
                    placeholder="Username"
                    onChangeText={this.onUsernameChange.bind(this)}
                />
            </CardSection>

            <CardSection>
                <Field
                    value={this.props.email}
                    label="Email"
                    placeholder="email@gmail.com"
                    onChangeText={this.onEmailChange.bind(this)}
                />
            </CardSection>

            <CardSection>
                <Field
                    secureTextEntry
                    label="Password"
                    placeholder="Password"
                    onChangeText={this.onPasswordChange.bind(this)}
                    value={this.props.password}
                />
            </CardSection>

            {this.renderError()}

            <CardSection>
                {this.renderButton()}
            </CardSection>

        </Card>
    }
}

const mapsStateToProps = ({ auth }) => {
    const { email, password, error, loading, username } = auth
    return { email, password, error, loading, username }
}

const styles = StyleSheet.create({
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        margin: 16
    }
})

export default connect(mapsStateToProps, { emailChanged, passwordChanged, signUpUser, usernameChanged })(SignUpForm)