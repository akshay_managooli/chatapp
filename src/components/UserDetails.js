import React, { PureComponent } from 'react'
import { StyleSheet, View, Text, Image ,TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'
import { usernameChanged, usernameUpdate ,fetchUsers} from '../actions'
import { Button, Card, CardSection, Field, Spinner } from './common'

class UserDetails extends PureComponent {

    componentDidUpdate(){
        const { userNameUpdated} = this.props 
        if (userNameUpdated) {
            this.props.fetchUsers({ isSetCurrentUser: true })
        }
    }

    onUserNameChanged(text) {
        this.props.usernameChanged(text)
    }

    onButtonPress() {
        const { username,existingUserName } = this.props
        this.props.usernameUpdate({ username ,existingUserName})
    }

    renderError() {
        if (this.props.error) {
            return <View>
                <Text style={styles.errorTextStyle}>{this.props.error}</Text>
            </View>
        }
    }

    renderButton() {
        if (this.props.loading) {
            return <Spinner size='small' pStyles={styles.spinnerStyle} />
        }
        return <Button title="Save" onPress={this.onButtonPress.bind(this)} />
    }

    renderProfileImageComponent() {
        return <TouchableOpacity style={styles.profileImageBackgroundStyle}>
            <Image
                style={styles.profileImageCameraIconStyle}
                source={require('../assets/photo_camera.png')} />
        </TouchableOpacity>
    }

    render() {
        return <Card>
            {this.renderProfileImageComponent()}
            <CardSection>
                <Field
                    value={this.props.username}
                    placeholder="E.g., John123"
                    label="Username"
                    onChangeText={this.onUserNameChanged.bind(this)} />
            </CardSection>
            {this.renderError()}

            <CardSection>
                {this.renderButton()}
            </CardSection>
        </Card>
    }
}

const styles = StyleSheet.create({
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
        margin: 16
    },
    profileImageBackgroundStyle: {
        height: 80,
        width: 80,
        alignSelf: 'center',
        backgroundColor: '#cccccc',
        borderRadius: 100,
        justifyContent:'center',
        marginTop:16,
        marginBottom:16
    },
    profileImageCameraIconStyle: {
        alignSelf: 'center'
    }
})

const mapsStateToProps = ({ auth }) => {
    const { error, loading, username ,existingUserName,userNameUpdated} = auth
    return { error, loading, username,existingUserName ,userNameUpdated}
}

export default connect(mapsStateToProps, { usernameChanged, usernameUpdate ,fetchUsers})(UserDetails)