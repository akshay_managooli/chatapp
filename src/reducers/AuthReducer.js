import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_FAIL,
    LOGIN_USER_SUCCESS,
    SPINNER_VISIBILITY,
    USERNAME_CHANGED,
    USERNAME_UPDATED,
    USERNAME_UPDATE_ERROR,
    EXISTING_USER_NAME
} from '../actions/Types'
const INITIAL_STATE = {
    username: '',
    email: '',
    password: '',
    user: null,
    error: '',
    loading: false,
    userNameUpdated:false
}
export default (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case EMAIL_CHANGED:
            return { ...state, email: action.payload }
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload }
        case USERNAME_CHANGED:
            return { ...state, username: action.payload }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                ...INITIAL_STATE,
                user: action.payload
            }
        case USERNAME_UPDATED:
            return {
                ...state,
                INITIAL_STATE,
                loading: false,
                userNameUpdated:true
            }
        case USERNAME_UPDATE_ERROR:
            return { ...state, error: action.payload ? action.payload : 'Username already taken, please try again!', loading: false }

        case LOGIN_USER_FAIL:
            return { ...state, error: action.payload ? action.payload : 'Authentication Failed', loading: false }
        case SPINNER_VISIBILITY:
            return { ...state, error: '', loading: true }
        default:
            return state
    }
}