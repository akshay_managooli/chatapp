import {
    SEND_CHAT_FAILED,
    SEND_CHAT_SUCCESSFULL,
    CHAT_CHANGED,
    CHATS_FETCH,
    FETCH_USERS,
    SPINNER_VISIBILITY_CHAT,
    CURRENT_USER,
    FETCH_ALL_CHATS,
    CLEAR_ALL,
    CLEAR_FETCHED_USERS
} from '../actions/Types'

const INITIAL_STATE = { message: '', chats: {}, isLoading: true, allusers: {}, currentUser: {}, allChats: {} }

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHATS_FETCH:
            return { ...state, chats: action.payload, isLoading: false }
        case CHAT_CHANGED:
            return { ...state, message: action.payload }
        case SEND_CHAT_SUCCESSFULL:
            return { ...state, message: '', isLoading: false }
        case FETCH_USERS:
            return { ...state, allusers: action.payload, isLoading: false }
        case CURRENT_USER:
            return { ...state, currentUser: action.payload.currentUser, allChats: action.payload.allChats, isLoading: false }
        case FETCH_ALL_CHATS:
            return { ...state, allChats: action.payload, isLoading: false }
        case SPINNER_VISIBILITY_CHAT:
            return { ...state, isLoading: true }
        case CLEAR_FETCHED_USERS:
            return { ...state, allusers: {} }
        case CLEAR_ALL:
            return INITIAL_STATE
        default:
            return state;
    }
}