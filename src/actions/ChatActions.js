import {
    SEND_CHAT_SUCCESSFULL,
    CHAT_CHANGED,
    CHATS_FETCH,
    FETCH_USERS,
    SPINNER_VISIBILITY_CHAT,
    CURRENT_USER,
    FETCH_ALL_CHATS,
    CLEAR_ALL,
    CLEAR_FETCHED_USERS
} from './Types'
import _ from 'lodash'
import firebase from 'firebase'
import { Actions } from 'react-native-router-flux'

export const chatChanged = (text) => {
    return {
        type: CHAT_CHANGED,
        payload: text
    }
}

export const sendChat = ({ text, chatId }) => {
    const { currentUser } = firebase.auth()

    return (dispatch) => {
        firebase.database().ref(`/chats/${chatId}/messages`)
            .push({ name: currentUser.displayName, message: text, id: currentUser.uid, date: new Date().toUTCString() })
            .then(() => {
                dispatch({ type: SEND_CHAT_SUCCESSFULL })
            })
    }
}

export const chatsFetch = ({ chatId }) => {
    const { currentUser } = firebase.auth()
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY_CHAT })
        firebase.database().ref(`/chats/${chatId}`)
            .on('value', snapshot => {
                dispatch({ type: CHATS_FETCH, payload: snapshot.val() })
            })
    }
}

export const acceptChatRequest = ({chatId}) =>{
    return (dispatch)=>{
        dispatch({type:SPINNER_VISIBILITY_CHAT})
        firebase.database().ref(`/chats/${chatId}`)
        .update({acceptedByRecepient:true})
    }
}

export const fetchUsers = ({ isSetCurrentUser }) => {
    const { currentUser } = firebase.auth()
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY_CHAT })
        firebase.database().ref('/users')
            .once('value', snapshot => {
                if (isSetCurrentUser) {
                    const allUsersList = _.map(snapshot.val(), (val, _id) => {
                        return { ...val, _id }
                    })

                    allUsersList.filter((item) => {
                        if (item.uid === currentUser.uid) {
                            firebase.database().ref(`/users/${item._id}/chats`)
                                .on('value', snapshot => {
                                    dispatch({
                                        type: CURRENT_USER, payload: {
                                            currentUser: item,
                                            allChats: snapshot.val()
                                        }
                                    })
                                })
                        }
                    })
                } else {
                    dispatch({ type: FETCH_USERS, payload: snapshot.val() })
                }
            })
    }
}

export const createChatRoom = ({ recepientUser, currentUserObjectid }) => {
    const { currentUser } = firebase.auth()
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY_CHAT })
        const { chats } = recepientUser
        const recepientUsersChats = _.map(chats, (val, _id) => {
            return { ...val, _id }
        })

        var privateChat = ''

        if (!_.isEmpty(recepientUsersChats)) {
            recepientUsersChats.filter((item) => {
                if (currentUser.displayName === item.chatName) {
                    privateChat = item.chatId
                    return true
                }
            })
        }
        if (privateChat !== '') {
            Actions.chatsScreen({ type: 'replace', chatId: privateChat })
            dispatch({ type: CLEAR_FETCHED_USERS })
        } else {

            firebase.database().ref(`/chats`)
                .push({
                    isPrivateChat: true,
                    date: new Date().toUTCString(),
                    invitedBy:currentUser.uid,
                    acceptedByRecepient:false,
                    members: [currentUser.uid, recepientUser.uid]
                })
                .then((value) => {
                    const result = value.toString()
                    const chatId = result.substring(result.lastIndexOf('/') + 1)
                    firebase.database().ref(`users/${currentUserObjectid}/chats`)
                        .push({
                            chatId: chatId,
                            chatName: recepientUser.username,
                            isPrivateChat: true
                        }).then(() => {
                            firebase.database().ref(`users/${recepientUser._id}/chats`)
                                .push({
                                    chatId: chatId,
                                    chatName: currentUser.displayName,
                                    isPrivateChat: true
                                }).then(() => {
                                    Actions.chatsScreen({ type: 'replace', chatId: chatId })
                                    dispatch({ type: CLEAR_FETCHED_USERS })
                                })
                        })
                })
        }
    }
}

export const fetchAllChats = ({ currentUser }) => {
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY_CHAT })
        firebase.database().ref(`/users/${currentUser._id}/chats`)
            .on('value', snapshot => {
                dispatch({ type: FETCH_ALL_CHATS, payload: snapshot.val() })
            })
    }
}

export const logout = () => {
    return (dispatch) => {
        firebase.auth().signOut()
            .then(() => {
                dispatch({ type: CLEAR_ALL })
            })
    }
}