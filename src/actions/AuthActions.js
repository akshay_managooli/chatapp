import {
    EMAIL_CHANGED,
    PASSWORD_CHANGED,
    LOGIN_USER_FAIL,
    LOGIN_USER_SUCCESS,
    SPINNER_VISIBILITY,
    USERNAME_CHANGED,
    USERNAME_UPDATED,
    USERNAME_UPDATE_ERROR,
    EXISTING_USER_NAME
} from './Types'
import _ from 'lodash'
import firebase from 'firebase'
import { Actions } from 'react-native-router-flux'
export const emailChanged = (text) => {
    return {
        type: EMAIL_CHANGED,
        payload: text
    }
}

export const passwordChanged = (text) => {
    return {
        type: PASSWORD_CHANGED,
        payload: text
    }
}

export const usernameChanged = (text) => {
    return {
        type: USERNAME_CHANGED,
        payload: text
    }
}

export const loginUser = ({ email, password }) => {
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY })
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(user => {
                loginUserSuccess(dispatch, user)
            })
            .catch((error) => {
                if (error.code === 'auth/wrong-password') {
                    loginUserFail(dispatch, error.message)
                } else {
                    firebase.auth().createUserWithEmailAndPassword(email, password)
                        .then(user => {
                            loginUserSuccess(dispatch, user)
                        })
                        .catch((error) => {
                            loginUserFail(dispatch, error.message)
                        })
                }
            })
    }
}

export const signUpUser = ({ email, password }) => {
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY })

        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user => {
                loginUserSuccess(dispatch, user)
            })
            .catch((error) => {
                loginUserFail(dispatch, error.message)
            })
    }
}

export const usernameUpdate = ({ username }) => {
    const { currentUser } = firebase.auth()
    return (dispatch) => {
        dispatch({ type: SPINNER_VISIBILITY })
        if (username === '') {
            dispatch({ type: USERNAME_UPDATE_ERROR, payload: 'Please enter valid username!!' })
        } else {
            firebase.database().ref(`/users`)
                .once('value', snapshot => {
                    if (snapshot.val()) {
                        const userList = _.map(snapshot.val(), (val, _id) => {
                            return { ...val, _id }
                        })
                        const userexists = _.filter(userList, { username }).length
                        const uid = currentUser.uid
                        const currentUserData = _.filter(userList, { uid })
                        if (userexists === 1) {
                            dispatch({ type: USERNAME_UPDATE_ERROR })
                        } else {
                            updateUserNameInChats(username, currentUserData[0].username)
                            updateUsername(dispatch, username, currentUser, currentUserData[0]._id)
                        }
                    } else {
                        dispatch({ type: USERNAME_UPDATE_ERROR, payload: 'Something went wrong!!' })
                    }
                })
        }
    }
}

const updateUsername = (dispatch, username, currentUser, _id) => {
    firebase.auth().currentUser.updateProfile({
        displayName: username
    }).then((s) => {
        firebase.database().ref(`/users/${_id}`)
            .update({ username, uid: currentUser.uid, email: currentUser.email })
            .then(() => {
                usernameUpdated(dispatch)
            })
    })
}

const updateUserNameInChats = (username, existingusername) => {

    // Updates usernames in chat list of users
    firebase.database().ref(`/users/`)
        .once('value', snapshot => {
            if (snapshot.val()) {
                const userList = _.map(snapshot.val(), (val, _id) => {
                    return { ...val, _id }
                })
                userList.forEach(element => {
                    firebase.database().ref(`/users/${element._id}/chats/`)
                        .once('value', snapshot => {
                            if (snapshot.val()) {
                                const chatList = _.map(snapshot.val(), (val, _id) => {
                                    return { ...val, _id }
                                })
                                const chats = _.filter(chatList, { chatName: existingusername, isPrivateChat: true })
                                chats.forEach(chatElement => {
                                    firebase.database().ref(`/users/${element._id}/chats/${chatElement._id}/`)
                                        .update({ chatName: username })
                                });
                            }
                        })
                });
            }
        })

    // Updates usernames in messages
    firebase.database().ref(`/chats/`)
        .once('value', snapshot => {
            if (snapshot.val()) {
                const chatList = _.map(snapshot.val(), (val, _id) => {
                    return { ...val, _id }
                })

                chatList.forEach(element => {
                    firebase.database().ref(`/chats/${element._id}/messages/`)
                        .once('value', snapshot => {
                            if (snapshot.val()) {
                                const messageList = _.map(snapshot.val(), (val, _id) => {
                                    return { ...val, _id }
                                })
                                const messages = _.filter(messageList, { name: existingusername })
                                messages.forEach(messageElement => {
                                    firebase.database().ref(`/chats/${element._id}/messages/${messageElement._id}/`)
                                        .update({ name: username })
                                });
                            }
                        })
                });
            }
        })

}

const loginUserSuccess = (dispatch, user) => {
    console.log(user)
    dispatch({ type: LOGIN_USER_SUCCESS, payload: user })
}

const loginUserFail = (dispatch, message) => {
    dispatch({ type: LOGIN_USER_FAIL, payload: message })
}

const usernameUpdated = (dispatch) => {
    dispatch({ type: USERNAME_UPDATED })
    Actions.main()
}
