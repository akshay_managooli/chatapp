import React from 'react'
import { Scene, Router } from 'react-native-router-flux'
import LoginForm from './components/LoginForm'
import SplashScreen from './components/SplashScreen'
import ChatsScreen from './components/ChatsScreen'
import SignUpForm from './components/SignUpForm'
import UserDetails from './components/UserDetails'
import ChatsMainListingScreen from './components/ChatsMainListingScreen'
import SearchUser from './components/SearchUser'
import { Image, TouchableOpacity } from 'react-native'
import { HomeScreenMenu } from './components/common/HomeScreenMenu'

const RouterComponent = () => {
    return <Router navigationBarStyle={{ backgroundColor: '#2196F3' }}
        titleStyle={{ color: '#fff' }}
        navBarButtonColor='#fff'
    >
        <Scene key="root" hideNavBar>
            <Scene key="splash">
                <Scene key="splashScreen"
                    component={SplashScreen}
                    initial
                    hideNavBar />
            </Scene>
            <Scene key="auth" type="reset">
                <Scene key="login"
                    component={LoginForm}
                    title="Login"
                    initial />
                <Scene key="signup"
                    component={SignUpForm}
                    title="Sign Up"
                />
                <Scene
                    key="userdetails"
                    component={UserDetails}
                    title="Profile"
                />
            </Scene>

            <Scene key="main" type="reset">
                <Scene
                    key="chatsMainListingScreen"
                    title="Chats"
                    component={ChatsMainListingScreen}
                    initial />
                <Scene
                    key="chatsScreen"
                    component={ChatsScreen}
                    title="Chats" />
                <Scene
                    key="searchUser"
                    component={SearchUser}
                    title="Select user" />
                <Scene
                    key="userdetails"
                    component={UserDetails}
                    title="Profile"
                />

            </Scene>
        </Scene>
    </Router>
}

export default RouterComponent