import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import ReduxThunk from 'redux-thunk'
import firebase from 'firebase'
import Router from './Router'
import { Actions } from 'react-native-router-flux'

class App extends PureComponent {

  componentDidMount() {
    var firebaseConfig = {
      apiKey: "AIzaSyCKyy5-eOx-gLJGuWZ1Eecst4fElJJJyL4",
      authDomain: "reactnativechat-13692.firebaseapp.com",
      databaseURL: "https://reactnativechat-13692-default-rtdb.firebaseio.com",
      projectId: "reactnativechat-13692",
      storageBucket: "reactnativechat-13692.appspot.com",
      messagingSenderId: "455439637847",
      appId: "1:455439637847:web:2d939a27631b36be5ff212",
      measurementId: "G-VV8CGKEYQ0"
    }
    // Initialize Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig)
    }

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        setTimeout(() => {
          if (user.displayName) {
            Actions.main()
          } else {
            Actions.userdetails()
          }
        }, 200)

      } else {
        Actions.auth()
      }
    })
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
    return <Provider store={store}>
      <Router />
    </Provider>
  }
}

export default App